# viddloader

## Description
Script for downloading and cutting clips

## Installation
Currently python (3.10) must be installed to run this. In the future it is possible that a version to download and run is made available.

### To run with python:
   ```shell
   cd /path/to/project/folder/
   ```

Install:
   ```shell
   python -m pip --install -r ./requirements.txt
   ```

Run:
   ```shell
    python.exe ./main.py -u 'https://www.youtube.com/watch?v=1_XB_XYarJU' -s '0:1:0' -e '0:1:10'
   ```
