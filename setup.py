import PyInstaller.__main__  # pylint: disable=import-error

PyInstaller.__main__.run([
    '--name=viddloader',
    '--clean',
    '-p=./',
    '--hidden-import=urllib',
    '--noconfirm',
    '--distpath=' + './install/',
    './main.py'
])
