import argparse
from datetime import datetime
from pathlib import Path
from pytube import YouTube
from moviepy.video.io.ffmpeg_tools import ffmpeg_extract_subclip


def on_progress(stream, chunk, bytes_remaining):
    """Callback function"""
    total_size = stream.filesize
    bytes_downloaded = total_size - bytes_remaining
    pct_completed = bytes_downloaded / total_size * 100
    print(f"Status: {round(pct_completed, 2)} %")


def download_video(url: str):
    yt = YouTube(url, on_progress_callback=on_progress)
    out = yt.streams.filter(progressive=True, file_extension='mp4').\
        order_by('resolution').desc().first().download()
    print(f"Download complete: {out}")
    return f'{yt.title}.mp4'


def create_clips(video_path: Path, times):
    print(video_path, times)
    zero = datetime.strptime('0:0:0', "%H:%M:%S")
    start = datetime.strptime(times[0], "%H:%M:%S")
    end = datetime.strptime(times[1], "%H:%M:%S")

    start_secs = round((start - zero).total_seconds())
    end_secs = round((end - zero).total_seconds())

    ffmpeg_extract_subclip(video_path, start_secs, end_secs, targetname=f"{video_path.parent / video_path.stem}_{start_secs}_{end_secs}.mp4")


def main():
    parser = argparse.ArgumentParser(description="download a video from youtube")
    parser.add_argument('--url', '-u', type=str, help='Url of video to download')
    parser.add_argument('--start', '-s', type=str, help='Start time for clip to create hh:mm:ss')
    parser.add_argument('--end', '-e', type=str, help='End time for clip to create hh:mm:ss')

    args = parser.parse_args()

    video_name = download_video(args.url)
    create_clips(Path.cwd() / video_name, [args.start, args.end])

if __name__ == '__main__':
    main()
