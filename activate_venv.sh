if test -f ".env/bin/activate"; then
  . .env/bin/activate
fi

if test -f ".env/Scripts/activate"; then
  . .env/Scripts/activate
fi
